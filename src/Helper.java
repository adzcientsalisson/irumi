package irumi;

import java.awt.geom.Point2D;

public abstract class Helper {
    public static double bulletSpeed(double power) {
        return 20 - 3 * power;
    }
    public static Point2D.Double project(Point2D startPoint, double distance, double angle) {
        return new Point2D.Double(
                startPoint.getX() + distance * Math.sin(angle), startPoint.getY() + distance * Math.cos(angle)
        );
    }
    public static double angle(Point2D point1, Point2D point2) {
        return Math.atan2(point2.getX()-point1.getX(), point2.getY()-point1.getY());
    }
}
