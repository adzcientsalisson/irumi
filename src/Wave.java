package irumi;

import java.awt.geom.Point2D;
import static irumi.Helper.*;

public class Wave {
    Point2D.Double sourceLocation, targetLocation;
    double firePower, targetBearing, targetHeading, targetSpeed;
    long fireTime;
    public Wave() { }
    public double getDistanceTraveled(long time) {
        return bulletSpeed(firePower) * (time - fireTime);
    }
    public double[] getVector(Point2D.Double hitLocation){
        double distance = hitLocation.distance(targetLocation);
        double angle = angle(targetLocation, hitLocation) - targetHeading;

        return new double[]{distance, angle};
    }
}
