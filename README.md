# Irumi Zoldyck

Um robô feito em robocode com movimento de risco mínimo e mira baseada em logs.

## Como se comporta

### Radar 

Ao detectar um inimigo, o Irumi:

1. Registra algumas de suas informações em um objeto do tipo Enemy;
1. Insere ou atualiza esse objeto com as novas informações em um LinkedHashMap, usando o nome do inimigo como chave;
1. Verifica se o inimigo escaneado é também o mais próximo que se tem registro;
1. Tenta atirar no inimigo mais próximo;
1. Checa se há algum tiro que poderia ter acertado o inimigo escaneado;
1. Muda o radar para a direção do inimigo que não é escaneado há mais tempo.

### Movimento de risco mínimo

Esse movimento consiste em:

1. Gerar um ponto várias vezes ao redor do robô, cada vez a uma distância e ângulo aleatórios;
1. Calcular qual ponto gerado tem menor risco;
1. Definir esse ponto como o próximo destino e se mover até lá;
1. Quando chegar ao destino, repetir o processo.

O cálculo de risco de cada ponto é dado da seguinte forma:

- Se inicia com risco relativo ao número de inimigos vivos e à distância até o ponto;
- A lista de inimigos é iterada, e o risco é aumentado de acordo com:
    - a razão entre a energia do inimigo e a energia de Irumi;
    - a perpendicularidade com o inimigo;
    - a distância do inimigo até o ponto.
- Após iterar, é adicionado um risco relacionado com a posição inicial do robô.

### Mirando e Atirando

Cada vez que Irumi atira, uma Wave é criada, ou seja, as informações do inimigo e do tiro são salvas. Em seguida:

- Quando um inimigo é escaneado, todas as suas Waves são atualizadas;
    - caso alguma Wave já tenha ultrapassado o inimigo, suas informações são guardadas na HashMap stats; 
    - junto com essas informações, é registrado como o inimigo se moveu no intervalo do tiro até o acerto.
- Ao mirar, Irumi tenta prever o inimigo com base na situação passada mais parecida com a atual;

## Pontos Fortes

- Devido à sua mira baseada em logs, Irumi pode lidar bem com movimentos previsíveis;
- Seu radar praticamente não desperdiça tempo de rotação;
- Seu movimento de mínimo risco o ajuda a sobreviver por um bom tempo.

## Pontos fracos

- Em disputas um contra um, Displacement Vector tende a ser menos eficiente do que seria GuessFactor Targeting;
- Apesar de ajudar a sobreviver, o seu movimento não detecta tiros e tenta desviar, então muitas vezes é acertado;
- A implementação do Dynamic Clustering é ineficiente se comparada com robôs como Diamond.

## Considerações

Nesta fase, aprofundei meus conhecimentos em java, aprendi algumas estruturas de dados, exercitei minha de leitura de código e criei noções básicas sobre Git e sobre Inteligência Artificial.