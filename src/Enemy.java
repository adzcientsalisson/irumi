package irumi;

import robocode.*;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;

import irumi.Wave;
import static irumi.Helper.project;


public class Enemy {
    ArrayList<Wave> waves;
    HashMap<double[], double[]> stats;
    Point2D.Double position;
    double energy, absBearing, heading, velocity;
    AdvancedRobot robot;
    public Enemy(AdvancedRobot r){
        robot = r;
        position = new Point2D.Double(30000,30000);
        stats = new HashMap<>();
        waves = new ArrayList<>();
        energy = 100;
    }
    public void update(ScannedRobotEvent e) {
        energy = e.getEnergy();
        absBearing = e.getBearingRadians() + robot.getHeadingRadians();
        heading = e.getHeadingRadians();
        Point2D.Double myPos = new Point2D.Double(robot.getX(), robot.getY());
        double eX, eY;
        eX = robot.getX() + e.getDistance() * Math.sin(absBearing);
        eY = robot.getY() + e.getDistance() * Math.cos(absBearing);
        position = project(myPos, e.getDistance(), absBearing);
        velocity = e.getVelocity();
    }
}
