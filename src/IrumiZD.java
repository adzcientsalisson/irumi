package irumi;

import robocode.AdvancedRobot;
import robocode.BulletHitEvent;
import robocode.RobotDeathEvent;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import static irumi.Helper.*;
import irumi.Wave;
import irumi.Enemy;

public class IrumiZD extends AdvancedRobot {
    static double minDist;
    static LinkedHashMap<String, Enemy> enemyList;
    static HashMap<String, Enemy> dead;
    static Rectangle2D.Double battleField, realBattleField;
    static {
        dead = new HashMap<>();
        enemyList = new LinkedHashMap<>(5, 2, true);
    }
    double scanDir, heading;
    Point2D.Double myCoords, next, lastPosition;
    Enemy closestEnemy;
    Object oldestScanned;
    public void run(){
        setAdjustGunForRobotTurn(true);
        setAdjustRadarForGunTurn(true);

        minDist = 0.7;
        battleField = new Rectangle2D.Double
                (30, 30, getBattleFieldWidth() -60, getBattleFieldHeight() -60);
        realBattleField = new Rectangle2D.Double(0, 0, getBattleFieldWidth(), getBattleFieldHeight());
        scanDir = 1;
        enemyList.putAll(dead);
        dead = new LinkedHashMap<>();
        lastPosition = next = myCoords = new Point2D.Double(getX(), getY());
        closestEnemy = new Enemy(this);

        setTurnRightRadians(Double.POSITIVE_INFINITY);
        while(true) {
            myCoords = new Point2D.Double(getX(), getY());
            heading = getHeadingRadians();
            setTurnRadarRightRadians(scanDir * Double.POSITIVE_INFINITY);
            doMovement();
            scan();
        }
    }
    public void onScannedRobot(ScannedRobotEvent e) {
        String eName = e.getName();

        Enemy thisEnm;
        if((thisEnm = enemyList.get(eName)) == null) thisEnm = new Enemy(this);

        thisEnm.update(e);
        enemyList.put(eName, thisEnm);

        if(closestEnemy == null || e.getDistance() < myCoords.distance(closestEnemy.position)){
            closestEnemy = thisEnm;
        }

        if(closestEnemy == thisEnm)
        doGun(e, closestEnemy);

        checkHit(thisEnm, thisEnm.position);
        out.println(thisEnm.stats.size());

        if ((eName.equals(oldestScanned) || oldestScanned == null) && enemyList.size() == getOthers()) {
            scanDir = Utils.normalRelativeAngle(
                    enemyList.values().iterator().next().absBearing - getRadarHeadingRadians()
            );
            oldestScanned = enemyList.keySet().iterator().next();
        }
    }
    public void onRobotDeath(RobotDeathEvent e) {
        String eName = e.getName();
        Enemy removed = enemyList.remove(eName);
        if(removed != null) {
            dead.put(eName, removed);

            if (removed == closestEnemy)
                closestEnemy = new Enemy(this);
        }
        oldestScanned = null;

    }
    public void onBulletHit(BulletHitEvent e){
        Enemy enm = enemyList.get(e.getName());

        Point2D.Double hitLocation = new Point2D.Double(e.getBullet().getX(), e.getBullet().getY());
        if(hitLocation.distance(enm.position) < 26)
            hitLocation = enm.position;

        if(!dead.containsKey(e.getName()))
            checkHit(enm, hitLocation);
    }
    public void doMovement() {
        if(next == null)
            next = myCoords;

        double nextDistance = myCoords.distance(next);
        double targetDistance = myCoords.distance(closestEnemy.position);

        if(nextDistance < 30){
            int i = 0;
            double addLast = 1 - Math.rint(Math.pow(Math.random(), getOthers()));

            do{
                Point2D.Double point = project(myCoords, Math.min(targetDistance*0.3, 100 + 200*Math.random()), 2*Math.PI*Math.random());

                if(battleField.contains(point) && getRisk(point,addLast) < getRisk(next,addLast)){
                    next = point;
                }

            }while(i++ < 300);
            lastPosition = myCoords;
        } else {
            double angle = angle(myCoords, next) - heading;
            double direction = 1;

            if(Math.cos(angle) < 0){
                angle += Math.PI;
                direction = -1;
            }

            angle = Utils.normalRelativeAngle(angle);
            setAhead(nextDistance * direction);
            setTurnRightRadians(angle);
            setMaxVelocity(Math.abs(angle) > 1 ? 2d : 8d);
        }
    }
    public double getRisk(Point2D.Double p, double addLast){
        double risk = addLast*0.08/p.distanceSq(lastPosition);

        for (Enemy e : enemyList.values()) {
            risk += Math.min(e.energy/getEnergy(),3) *
                    (1 + Math.abs(Math.cos(angle(myCoords, p) - angle(e.position, p)))) / p.distanceSq(e.position);;
        }
        risk += Math.random()/5/myCoords.distanceSq(p);
        return risk;
    }
    public void doGun(ScannedRobotEvent e, Enemy enm){
        double power = Math.min(Math.min(3, 800/e.getDistance()), Math.min(getEnergy(), enm.energy)/4);

        if(getGunTurnRemainingRadians() == 0) {
            if(getGunHeat() == 0 && getEnergy() > 1) {
                setFire(power);
                Wave w = new Wave();
                w.sourceLocation = myCoords;
                w.targetLocation = enm.position;
                w.fireTime = getTime();
                w.firePower = power;
                w.targetHeading = enm.heading;
                w.targetBearing = enm.absBearing;
                w.targetSpeed = e.getVelocity();

                enm.waves.add(w);
            }

            double latVel = enm.velocity * Math.sin(enm.absBearing);
            double direction = enm.velocity >= 0 ? 0 : Math.PI;
            double eVectorAngle = 0;
            double[] closest = {0, 0};
            double closestDistance = 1;
            double[] current = {
                    Math.abs(enm.velocity) / 8,
                    Math.abs(latVel)/8,
                    Utils.normalAbsoluteAngle(direction + enm.heading - enm.absBearing) / (4 * Math.PI),
                    Math.min(2, (e.getDistance() / bulletSpeed(power)) / 50f),
                    enm.position.getX() / getBattleFieldWidth() * minDist * minDist,
                    enm.position.getY() / getBattleFieldHeight() * minDist * minDist
            };

            for (double[] dVector : enm.stats.keySet()) {
                double[] point = enm.stats.get(dVector);
                double distanceSq = 0;
                for (int i = 0; i < current.length; i++) {
                    distanceSq += (point[i] - current[i]) * (point[i] - current[i]);
                }
                eVectorAngle = dVector[1] + enm.heading;
                if (Math.sqrt(distanceSq) < closestDistance &&
                        realBattleField.contains(project(enm.position, dVector[0], eVectorAngle))) {
                    closest = dVector;
                    closestDistance = distanceSq;
                }
            }
            Point2D.Double predictedPos = project(enm.position, closest[0], eVectorAngle);
            double absAngle = angle(myCoords, predictedPos);
            setTurnGunRightRadians(Utils.normalRelativeAngle(absAngle - getGunHeadingRadians()));
        }
    }
    public void checkHit(Enemy enm, Point2D.Double hitLocation){
        for(Iterator<Wave> it = enm.waves.iterator(); it.hasNext();){
            Wave w = it.next();
            if(w.sourceLocation.distance(hitLocation) + 25 < w.getDistanceTraveled(getTime())){
                double speed = (w.targetSpeed+8) / 8;
                double lateralVelocity = Math.abs(Math.sin(w.targetBearing) * w.targetSpeed) / 8;
                double direction = w.targetSpeed >= 0 ? 0 : Math.PI;
                double relHeading = Utils.normalAbsoluteAngle(direction + w.targetHeading - w.targetBearing) / (4*Math.PI);
                double flightTime = Math.min(2, (getTime() - w.fireTime) / 50f);
                double targetX = w.sourceLocation.getX()/getBattleFieldWidth() * minDist * minDist;
                double targetY = w.sourceLocation.getY()/getBattleFieldHeight() * minDist * minDist;

                double[] dVector = w.getVector(hitLocation);

                enm.stats.put(dVector,
                        new double[]{speed, lateralVelocity, relHeading, flightTime, targetX, targetY});
                it.remove();
            }
        }
    }
}